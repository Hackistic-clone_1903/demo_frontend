import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static final ThemeData lightTheme = ThemeData(
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: AppBarTheme(
      color: Colors.white,
      iconTheme: IconThemeData(color: Color(0xFF30475E)),
    ),
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    textTheme: TextTheme(
      headline6: TextStyle(
        color: Colors.black,
        fontSize: 24,
      ),
      subtitle1: TextStyle(
        color: Colors.black,
        fontSize: 18,
      ),
      subtitle2: TextStyle(
        color: Colors.black,
        fontSize: 14,
      ),
    ),
    shadowColor: Colors.black54,
    colorScheme: ColorScheme.light(
        primary: Colors.orangeAccent, secondary: Colors.deepOrange),
  );

  static final ThemeData darkTheme = ThemeData(
    scaffoldBackgroundColor: Color(0xFF30475E),
    appBarTheme: AppBarTheme(
      color: Color(0xFF30475E),
      iconTheme: IconThemeData(color: Colors.white),
    ),
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    textTheme: TextTheme(
      headline6: TextStyle(
        color: Colors.white,
        fontSize: 24,
      ),
      subtitle1: TextStyle(
        color: Colors.white,
        fontSize: 18,
      ),
      subtitle2: TextStyle(
        color: Colors.white,
        fontSize: 14,
      ),
    ),
    shadowColor: Colors.white70,
    colorScheme: ColorScheme.dark(
        primary: Colors.blueAccent, secondary: Colors.blue[900]),
  );
}
