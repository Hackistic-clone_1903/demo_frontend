import 'package:flutter/material.dart';

class ThemeNotifier extends ChangeNotifier {
  bool isDark = false;

  void updateTheme(bool isDark) {
    this.isDark = isDark;
    notifyListeners();
  }
}
