import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:claimz_app/Pages/apply_claims.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    ),
  );
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  double fonts = 18.0;

  void changeFont() async {
    setState(() {
      if (fonts >= 18.0) {
        fonts = 18.0;
      } else {
        fonts = fonts + 1.0;
      }
    });
  }

  void decreaseFont() {
    setState(() {
      if (fonts <= 15.0) {
        fonts = 15.0;
      } else {
        fonts = fonts - 1.0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(left: 53.0, top: 230.0),
              height: 260,
              width: 290,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  color: Color(0xfff3efef)),
            ),
            Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.only(top: 10.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'ClaimZ',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 24.0,
                                fontWeight: FontWeight.bold),
                          ),
                          Image.asset(
                            'assets/images/Logo1.png', //ClaimZ APP lOGO
                            height: 75.0,
                            width: 55.0,
                            fit: BoxFit.contain,
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Insurance claim made easy',
                            style: TextStyle(
                              color: Color(0xff367BDA),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 140.0, right: 205.0),
                              child: Text(
                                'Email',
                                style: TextStyle(
                                  fontSize: fonts,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 70.0, right: 70.0, top: 10.0),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(15.0),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                ),
                                validator: (value) =>
                                    EmailValidator.validate(value)
                                        ? null
                                        : "Please enter a valid email",
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0, right: 180.0),
                              child: Text(
                                'Password',
                                style: TextStyle(
                                    fontSize: fonts,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 70.0, right: 70.0, top: 10.0),
                              child: TextFormField(
                                obscureText: true,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(15.0),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                ),
                                validator: (password) {
                                  if (!RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                                          .hasMatch(password) ||
                                      password.length < 8) {
                                    return 'Please Validate Your Password';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0, left: 70.0),
                              child: Row(
                                children: [
                                  RaisedButton(
                                    child: Text(
                                      'Login',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      // Validate returns true if the form is valid, or false otherwise.
                                      if (_formKey.currentState.validate()) {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) => Apply_Claims()));
                                      }
                                    },
                                    color: Color(0xffdf5b03),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(8.0)),
                                  ),
                                  SizedBox(
                                    width: 20.0,
                                  ),
                                  Text(
                                    'Forgot Password?',
                                    style: TextStyle(
                                        color: Color(0xff205292),
                                        fontWeight: FontWeight.bold,
                                        fontSize: fonts),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15.0, top: 40.0),
                            child: RaisedButton(
                              child: Text(
                                'New User?',
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {},
                              color: Color(0xff367BDA),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0)),
                            ),
                          ),
                          SizedBox(
                            width: 100.0,
                          ),
                          Container(
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () {
                                    decreaseFont();
                                  },
                                  child: Container(
                                    padding:
                                        EdgeInsets.only(left: 20.0, top: 20.0),
                                    child: Row(
                                      children: [
                                        Image.asset('assets/images/A-font.png'),
                                      ],
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    changeFont();
                                  },
                                  child: Container(
                                    padding:
                                        EdgeInsets.only(left: 20.0, top: 20.0),
                                    child: Row(
                                      children: [
                                        Image.asset('assets/images/A+font.png'),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 125.0, left: 10.0),
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      Icons.help_outline_rounded,
                                      color: Color(0xffB0B5CA),
                                      size: 30.0,
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: Text(
                                        'Help',
                                        style: TextStyle(
                                          color: Color(0xffB0B5CA),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10.0),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.language_outlined,
                                        color: Color(0xffB0B5CA),
                                        size: 30.0,
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: Text(
                                          'Language',
                                          style: TextStyle(
                                              color: Color(0xffB0B5CA)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 170.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Image.asset('assets/images/Power-logo.png'),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
