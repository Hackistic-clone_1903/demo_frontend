import 'package:flutter/material.dart';
import 'package:custom_switch/custom_switch.dart';
import 'package:provider/provider.dart';
import 'package:claimz_app/theme/theme_notifier.dart';

void main() {
  runApp(
    EditSettingPage(),
  );
}

class EditSettingPage extends StatefulWidget {
  @override
  _EditSettingPageState createState() => _EditSettingPageState();
}

class _EditSettingPageState extends State<EditSettingPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Settings',
      home: Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(120),
          child: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Theme.of(context).colorScheme.secondary,
                    Theme.of(context).colorScheme.primary
                  ],
                ),
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 15.0),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 6.0, top: 95.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.settings,
                            color: Theme.of(context).iconTheme.color,
                            size: 32,
                          ),
                          SizedBox(
                            width: 6.0,
                          ),
                          Row(
                            children: [
                              Text(
                                'Settings',
                                style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .color,
                                    fontWeight: FontWeight.bold,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .fontSize),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: 15.0, top: 15.0, bottom: 10.0),
                        child: Text(
                          'Language',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.subtitle1.color,
                            fontSize:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.blue,
                    height: 30.0,
                    thickness: 4.0,
                  ),
                  Row(
                    children: [
                      Container(
                        padding:
                            EdgeInsets.only(left: 15, top: 15.0, bottom: 10.0),
                        child: Text(
                          'App Permissions',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.subtitle1.color,
                            fontSize:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.blue,
                    height: 30.0,
                    thickness: 4.0,
                  ),
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: 15.0, top: 15.0, bottom: 10.0),
                        child: Text(
                          'Notifications',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.subtitle1.color,
                            fontSize:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.blue,
                    height: 30.0,
                    thickness: 4.0,
                  ),
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: 15.0, top: 15.0, bottom: 10.0),
                        child: Text(
                          'App Lock',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.subtitle1.color,
                            fontSize:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.blue,
                    height: 30.0,
                    thickness: 4.0,
                  ),
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: 15.0, top: 15.0, bottom: 10.0),
                        child: Text(
                          'Dark Mode',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.subtitle1.color,
                            fontSize:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 175.0,
                      ),
                      //Theme changer switch
                      CustomSwitch(
                        activeColor: Color(0xff00BCD4),
                        value: Provider.of<ThemeNotifier>(context).isDark,
                        onChanged: (boolVal) {
                          Provider.of<ThemeNotifier>(context, listen: false)
                              .updateTheme(
                                  boolVal); //update theme call from theme notifier
                        },
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.blue,
                    height: 30.0,
                    thickness: 4.0,
                  ),
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: 15.0, top: 15.0, bottom: 10.0),
                        child: Text(
                          'Info',
                          style: TextStyle(
                            color: Theme.of(context).textTheme.subtitle1.color,
                            fontSize:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.blue,
                    height: 30.0,
                    thickness: 4.0,
                  ),
                ],
              ),
            ),
            Positioned(
              left: 20,
              bottom: 20,
              child: FloatingActionButton(
                backgroundColor: Colors.deepOrange,
                child: Icon(
                  Icons.chevron_left,
                  color: Colors.black,
                  size: 30,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
