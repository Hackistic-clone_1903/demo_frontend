import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/services.dart';
import 'package:claimz_app/Pages/settings.dart';



class Apply_Claims extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Apply_Claims> {
  bool val = false;
  double fonts = 18;
  DateTime date = DateTime.now();

  void changeFont() async {
    setState(() {
      if (fonts >= 18.0) {
        fonts = 18.0;
      } else {
        fonts = fonts + 1.0;
      }
    });
  }

  void decreaseFont() {
    setState(() {
      if (fonts <= 15.0) {
        fonts = 15.0;
      } else {
        fonts = fonts - 1.0;
      }
    });
  }

  void refresh() {
    setState(() {});
  }

  var policyNumbers = ['Zen001', 'Zen002', 'Zen003', 'Zen004'];
  var policy = 'Zen001';
  var cityNames = ['Tiruppur', 'Coimbatore', 'Erode', 'Chennai'];
  var city = 'Tiruppur';
  var hospitalNames = ['TP GH', 'CBE GH', 'ER GH'];
  var hospital = 'TP GH';

  File imageFile;

  gallery() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {});
  }

  camera() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {});
  }

  Future<void> _dialogmessage(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Choose '),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text('Gallery'),
                    onTap: () {
                      gallery();
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                  GestureDetector(
                    child: Text('Camera'),
                    onTap: () {
                      camera();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  DateTime newdate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: newdate,
        firstDate: new DateTime(2016),
        lastDate: new DateTime(2022));

    if (picked != null && picked != newdate) {
      setState(() {
        newdate = picked;
        print(newdate.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(125),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.orangeAccent, Colors.deepOrange]),
            ),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 20.0, top: 20.0),
                  child: Row(
                    children: [
                      Text(
                        'ClaimZ',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 24.0,
                        ),
                      ),
                      Image.asset(
                        'assets/images/Logo1.png',
                        height: 75.0,
                        width: 45.0,
                        fit: BoxFit.contain,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Row(
                    children: [
                      Image.asset('assets/images/appimg.png'),
                      Row(
                        children: [
                          Text(
                            'Apply ClaimZ',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: fonts),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          iconTheme: IconThemeData(
            color: Color(0xff1F70DE),
          ),
        ),
      ),
      endDrawer: Drawer(
        elevation: 1.0,
        child: Column(
          children: [
            Container(
              height: 150.0,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.orangeAccent, Colors.deepOrange])),
              padding: EdgeInsets.only(top: 22),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'ClaimZ',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 24.0,
                    ),
                  ),
                  Image.asset(
                    'assets/images/Logo1.png',
                    height: 75.0,
                    width: 45.0,
                    fit: BoxFit.contain,
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.only(left: 10, top: 20, bottom: 20),
                child: Row(
                  children: [
                    Icon(
                      Icons.account_circle_rounded,
                      color: Color(0xffE26E20),
                      size: 45.0,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Profile',
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: fonts,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => EditSettingPage()));
              },
              child: Container(
                padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                child: Row(
                  children: [
                    Icon(
                      Icons.settings,
                      color: Color(0xffE26E20),
                      size: 45.0,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Settings',
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: fonts,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(),
            InkWell(
              onTap: () {
              },
              child: Container(
                padding: EdgeInsets.only(left: 10, top: 20, bottom: 20),
                child: Row(
                  children: [
                    Icon(
                      Icons.map,
                      color: Color(0xffE26E20),
                      size: 45.0,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Maps',
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: fonts,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(),
            Container(
              padding: EdgeInsets.only(left: 10.0),
              child: Row(
                children: [
                  SafeArea(
                    child: Text(
                      'Change Text Size',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      decreaseFont();
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 20.0, top: 10.0),
                      child: Row(
                        children: [
                          Image.asset('assets/images/A-font.png'),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      changeFont();
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 20.0, top: 10.0),
                      child: Row(
                        children: [Image.asset('assets/images/A+font.png')],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topRight: Radius.circular(50))),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(left: 0.0, right: 140.0, top: 20.0),
                child: Text(
                  'Client ID/Policy No',
                  style: TextStyle(fontSize: fonts),
                ),
              ),
              Container(
                child: SizedBox(
                  width: 300,
                  child: DropdownButton<String>(
                    isExpanded: true,
                    items: policyNumbers.map((String policyNum) {
                      return DropdownMenuItem<String>(
                        value: policyNum,
                        child: Text(policyNum),
                      );
                    }).toList(),
                    onChanged: (String newValue) {
                      setState(() {
                        this.policy = newValue;
                        print(policy);
                      });
                    },
                    value: policy,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 0.0, right: 250.0, top: 20.0),
                child: Text(
                  'Name',
                  style: TextStyle(fontSize: fonts),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 50.0, right: 50.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 0.0, right: 250.0, top: 20.0),
                child: Text(
                  'Illness',
                  style: TextStyle(fontSize: fonts),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 0.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 50.0),
                child: Row(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20.0, top: 20.0),
                            child: Text(
                              'City',
                              style: TextStyle(fontSize: fonts),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 50.0, top: 20.0),
                            child: SizedBox(
                              width: 110,
                              child: DropdownButton<String>(
                                isExpanded: true,
                                items: cityNames.map((String city) {
                                  return DropdownMenuItem<String>(
                                    value: city,
                                    child: Text(city),
                                  );
                                }).toList(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    this.city = newValue;
                                    print(city);
                                  });
                                },
                                value: city,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 40.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 40.0, top: 20.0),
                            child: Text(
                              'Hospital',
                              style: TextStyle(fontSize: fonts),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 25.0),
                            child: SizedBox(
                              width: 130,
                              child: DropdownButton<String>(
                                isExpanded: true,
                                items: hospitalNames.map((String hospital) {
                                  return DropdownMenuItem<String>(
                                    value: hospital,
                                    child: Text(hospital),
                                  );
                                }).toList(),
                                onChanged: (String newValue) {
                                  setState(() {
                                    this.hospital = newValue;
                                    print(hospital);
                                  });
                                },
                                value: hospital,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 0.0, right: 180.0, top: 20.0),
                child: Text(
                  'Claim Amount',
                  style: TextStyle(fontSize: fonts),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 50.0, right: 50.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.red),
                    ),
                  ),
                  keyboardType: TextInputType.number,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 0.0, right: 250.0, top: 20.0),
                child: Text(
                  'Date',
                  style: TextStyle(fontSize: fonts),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 50.0, right: 50.0),
                child: InkWell(
                  onTap: () {
                    _selectDate(
                        context); // Call Function that has showDatePicker()
                  },
                  child: IgnorePointer(
                    child: new TextFormField(
                      decoration: new InputDecoration(
                        hintText: 'Select Date',
                        suffixIcon: IconButton(
                            icon: Icon(Icons.date_range), onPressed: () {}),
                      ),
                      // validator: validateDob,
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10.0, right: 20.0, top: 20.0),
                child: Text(
                  'Upload Receipt and Prescription:',
                  style: TextStyle(fontSize: fonts),
                ),
              ),
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(70.0)),
                padding: EdgeInsets.only(top: 20.0, left: 47.0),
                child: Row(
                  children: [
                    RaisedButton(
                      color: Colors.red,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0)),
                      child: Text(
                        'Upload',
                        style: TextStyle(fontSize: fonts),
                      ),
                      onPressed: () {
                        _dialogmessage(context);
                      },
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    RaisedButton(
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0)),
                      child: Text(
                        'Save',
                        style: TextStyle(fontSize: fonts),
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
