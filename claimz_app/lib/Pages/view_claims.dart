/* Claims History Page(Claimz App)
 * @author : Ajithkumar S (Zenitus Technology)
 * @Date: 2021/03/29
 * @Last Modified by: Ajithkumar S (Zenitus Technology)
 * @Last Modified Date: 2021/04/14 */
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
class claims_history extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(120),
        child: AppBar(//AppBar codes here
          flexibleSpace: Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:25.0,left:20),
                  child: Row(
                    children: [
                      Text('ClaimZ',
                            style: TextStyle(
                              fontSize: 28,
                              fontWeight: FontWeight.bold,
                            ),),

                        Image.asset('assets/Images/Logo1.png',
                        fit: BoxFit.contain,
                        height: 35,),
                    ],//Company title with logo
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:20.0,top:20),
                  child: Row(
                    children: [
                      Image.asset('assets/Images/history.png',
                        fit:BoxFit.contain,
                        height: 40,),

                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0),
                          child: Text("Claims History",
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),),
                        ),
                      )//page title
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body:Container(
        child:Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children:[
                Container(
               child: Column(
                children:[
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children:[
                    Container(
                      height:60,
                    width: 300,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.orange),
                      color: Colors.white
                    ),
                    child: Column(
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Container(
                                child: Row(
                                  children:[
                                  Container(
                                    child:Padding(
                                      padding: const EdgeInsets.only(left:3.0),
                                      child: Text('02-02-2021'),
                                    ) ,
                                  ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Container(
                                      child:Padding(
                                        padding: const EdgeInsets.only(left: 62),
                                        child: Text('Policy No:123xxxxxxxxx1'),
                                      ) ,
                                    ),
                               ],
                                ),
                              ),
                              SizedBox(
                                height: 14,
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:[
                                    Container(
                                      child:Padding(
                                        padding: const EdgeInsets.only(bottom:8.0),
                                        child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                      ) ,

                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )

                  ),
                    InkWell(
                      onTap: (){
                        showDialog(
                        context: context,builder: (context){
                          return Dialog(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                            elevation: 16,
                            child: Container(
                              height: 400,
                              width: 360,
                              child:Center(
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left:260.0),
                                      child: Container(
                                        child: CloseButton(
                                          onPressed: (){
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top:30.0,left:15.0),
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                Text("Claim No: xxxxxxxxx",
                                                  style: TextStyle(fontSize: 20),)
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Container(
                                            child: Row(
                                              children: [
                                                Text("Name: Jai",
                                                  style: TextStyle(fontSize: 20),)
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Container(
                                            child: Row(
                                              children: [
                                                Text("Date of Claim: mm-dd-yyyy",
                                                  style: TextStyle(fontSize: 20),)
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Container(
                                            child: Row(
                                              children: [
                                                Text("Status: Pending...",
                                                  style: TextStyle(fontSize: 20),)
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Container(
                                            child: Row(
                                              children: [
                                                Text("Illness: Elbow Fracture",
                                                  style: TextStyle(fontSize: 20),)
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Container(
                                            child: Row(
                                              children: [
                                                Text("Hospital: Tokyo Private Hospital",
                                                  style: TextStyle(fontSize: 20),)
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    // ignore: deprecated_member_use
                                    RaisedButton(
                                        child: Text('Download'),
                                        onPressed: (){}),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }
                        );
                      },//Client apply form with full details in popup box //client apply status box
                      child: Container(
                        height:60,
                        width: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.orange),
                          color: Colors.white,
                        ),
                        child: Container(
                          margin: EdgeInsets.all(10),
                            child: Image.asset('assets/Images/pending.png')

                      ),
                  ),//client apply status box
                    ),
                  ],
                ),
              ),
                ],
    ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.red),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),//Client apply form full details popup box
                            Container(
                                height:60,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.red),
                                  color: Colors.white,
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(15.0),
                                    child: Image.asset('assets/Images/x.png'))

                            ),//client apply status box
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.green),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),//Client apply form full details popup box
                            Container(
                                height:60,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.green),
                                  color: Colors.white,
                                ),
                                child:Container(
                                  margin:EdgeInsets.all(12),
                                child:Image.asset('assets/Images/o.png'),

                            ),
                            ),//client apply status box
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.orange),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),//Client apply form full details popup box
                            Container(
                              height:60,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.orange),
                                color: Colors.white,
                              ),
                              child: Container(
                                  margin: EdgeInsets.all(10),
                                  child: Image.asset('assets/Images/pending.png')

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.red),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                                height:60,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.red),
                                  color: Colors.white,
                                ),
                                child: Container(
                                    margin: EdgeInsets.all(15.0),
                                    child: Image.asset('assets/Images/x.png'))

                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.green),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                              height:60,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.green),
                                color: Colors.white,
                              ),
                              child:Container(
                                margin:EdgeInsets.all(12),
                                child:Image.asset('assets/Images/o.png'),

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.orange),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                              height:60,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.orange),
                                color: Colors.white,
                              ),
                              child: Container(
                                  margin: EdgeInsets.all(10),
                                  child: Image.asset('assets/images/pending.png')

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.red),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                                height:60,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.red),
                                  color: Colors.white,
                                ),
                                child: Container(
                                    margin: EdgeInsets.all(15.0),
                                    child: Image.asset('assets/Images/x.png'))

                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.green),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                              height:60,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.green),
                                color: Colors.white,
                              ),
                              child:Container(
                                margin:EdgeInsets.all(12),
                                child:Image.asset('assets/Images/o.png'),

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.orange),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                              height:60,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.orange),
                                color: Colors.white,
                              ),
                              child: Container(
                                  margin: EdgeInsets.all(10),
                                  child: Image.asset('assets/Images/pending.png')

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.red),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                                height:60,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.red),
                                  color: Colors.white,
                                ),
                                child: Container(
                                    margin: EdgeInsets.all(15.0),
                                    child: Image.asset('assets/Images/x.png'))

                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children:[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children:[
                            Container(
                                height:60,
                                width: 300,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(color: Colors.green),
                                    color: Colors.white
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Row(
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left:3.0),
                                                    child: Text('02-02-2021'),
                                                  ) ,
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(left: 62),
                                                    child: Text('Policy No:123xxxxxxxxx1'),
                                                  ) ,
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 14,
                                          ),
                                          Container(
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children:[
                                                Container(
                                                  child:Padding(
                                                    padding: const EdgeInsets.only(bottom:8.0),
                                                    child: Text('Illness/Hospital: Elbow fracture/Hos...'),
                                                  ) ,

                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                )

                            ),
                            Container(
                              height:60,
                              width: 80,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.green),
                                color: Colors.white,
                              ),
                              child:Container(
                                margin:EdgeInsets.all(12),
                                child:Image.asset('assets/Images/x.png'),

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
             ],
            ),
          ),
          Positioned(
            left: 20,
            bottom: 30,
            child: FloatingActionButton(
              onPressed: (){},
                child:Icon(
                  Icons.chevron_left,
                  color: Colors.black,
                )
            ),
          ),
        ],
      ),
      ),
    );
  }
}
