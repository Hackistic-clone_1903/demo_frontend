import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

class ViewBalance extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ViewBalance',
      home: ViewBalancePage(),
    );
  }
}

class ViewBalancePage extends StatefulWidget {

  @override
  _ViewBalanceState createState() => _ViewBalanceState();
}

class _ViewBalanceState extends State<ViewBalancePage> {

  Future<String> getData() async{
    http.Response response = await http.get(
      Uri.encodeFull("https://jsonplaceholder.typicode.com/posts"),
      headers: {
        "Accept": "application/json"
      }
    );
    print(response.body);


}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      ///appbar content
      floatingActionButton: FloatingActionButton(
        onPressed: getData,
        backgroundColor: Colors.blue,
        child: Icon(Icons.arrow_upward_outlined),
      ),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(125),
        child: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.deepOrange,
          flexibleSpace: Container(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 20.0, top: 20.0),
                  child: Row(
                    children: [
                      Text(
                        'ClaimZ',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                        ),
                      ),
                      Image.asset(
                        'assets/Logo1.png',
                        height: 32.0,
                        width: 45.0,
                        fit: BoxFit.contain,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 15.0,top: 30.0),
                  child: Row(
                    children: [
                      Image.asset('assets/clipboard.png',height: 40,),
                      SizedBox(
                        width: 10.0,
                      ),
                      Row(
                        children: [
                          Text(
                            'View Balance',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),

      ///ViewBalance Body
      body: MyCardWidget(),
    );
  }
}

class MyCardWidget extends StatelessWidget {
  MyCardWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [

          ///Card 1
          Container(
            width: 430,
            height: 400,
            padding: new EdgeInsets.all(10.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
              color: Color(0xffE7F1FF),
              elevation: 10,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 15.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Policy No:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Plan:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 1.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                  hintText: "123456789",
                                  border: InputBorder.none),
                              readOnly: true,
                            ),
                          ),
                          flex: 2,
                        ),
                        Flexible(
                          child: new TextField(
                            decoration: const InputDecoration(
                                hintText: "abc@gmail.com",
                                border: InputBorder.none),
                            readOnly: true,
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'First Name:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Last Name:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                hintText: "Shin",
                                border: InputBorder.none,
                              ),
                              readOnly: true,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          flex: 2,
                        ),
                        Flexible(
                          child: new TextField(
                            decoration: const InputDecoration(
                                hintText: "Chan", border: InputBorder.none),
                            readOnly: true,
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Allotted Amount:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Client ID:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                  hintText: "¥ 500000",
                                  border: InputBorder.none),
                              readOnly: true,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          flex: 2,
                        ),
                        Flexible(
                          child: new TextField(
                            decoration: const InputDecoration(
                                hintText: "123456789",
                                border: InputBorder.none),
                            readOnly: true,
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Balance Amount:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                  hintText: "¥ 300000",
                                  border: InputBorder.none),
                              readOnly: true,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),

          ///Card 2
          Container(
            width: 430,
            height: 400,
            padding: new EdgeInsets.all(10.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
              color: Color(0xffE7F1FF),
              elevation: 10,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 15.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Policy No:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Plan:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 1.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                  hintText: "123456789",
                                  border: InputBorder.none),
                              readOnly: true,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          flex: 2,
                        ),
                        Flexible(
                          child: new TextField(
                            decoration: const InputDecoration(
                                hintText: "abc@gmail.com",
                                border: InputBorder.none),
                            readOnly: true,
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'First Name:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Last Name:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                  hintText: "Shin", border: InputBorder.none),
                              readOnly: true,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          flex: 2,
                        ),
                        Flexible(
                          child: new TextField(
                            decoration: const InputDecoration(
                                hintText: "Chan", border: InputBorder.none),
                            readOnly: true,
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Allotted Amount:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Client ID:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                  hintText: "¥ 500000",
                                  border: InputBorder.none),
                              readOnly: true,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          flex: 2,
                        ),
                        Flexible(
                          child: new TextField(
                            decoration: const InputDecoration(
                                hintText: "123456789",
                                border: InputBorder.none),
                            readOnly: true,
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(left: 25.0, right: 25.0, top: 25.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: new Text(
                              'Balance Amount:',
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: new TextField(
                              decoration: const InputDecoration(
                                  hintText: "¥ 300000",
                                  border: InputBorder.none),
                              readOnly: true,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
