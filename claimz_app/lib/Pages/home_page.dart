import 'package:claimz_app/Widgets/end_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:claimz_app/Widgets/insurance_card.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        endDrawer: EndDrawer(),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70),
          child: AppBar(
            backgroundColor: Theme.of(context).appBarTheme.color,
            flexibleSpace: Container(
              padding: EdgeInsets.only(top: 18, left: 14),
              child: Row(
                children: <Widget>[
                  Text(
                    'ClaimZ',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Image.asset(
                    'assets/Images/ZenitusLogo.png',
                    fit: BoxFit.contain,
                    height: 36,
                  )
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: IconButton(
                    icon: Icon(
                      Icons.notifications_outlined,
                      size: 35,
                    ),
                    onPressed: () {}),
              ),
              Builder(
                builder: (context) => Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: IconButton(
                    icon: Icon(
                      Icons.menu,
                      size: 35,
                    ),
                    onPressed: () {
                      Scaffold.of(context).openEndDrawer();
                    },
                  ),
                ),
              )
            ],
          ),
        ),
        body: Center(
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Opacity(
                    opacity: 0.1,
                    child: Center(
                      child: Image.asset('assets/Images/ZenitusLogo.png',
                          height: 286, width: 250),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40.0, left: 30),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 5,
                      ),
                      InsCard(
                        color1: Colors.purple,
                        color2: Colors.red,
                      ),
                      SizedBox(
                        width: 40,
                      ),
                      InsCard(
                        color1: Colors.red,
                        color2: Colors.purple,
                      ),
                      SizedBox(
                        width: 40,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 300.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          gradient: LinearGradient(colors: [
                            Colors.blue.shade900,
                            Colors.blue.shade700
                          ]),
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(context).shadowColor,
                              blurRadius: 2,
                              offset: Offset(3, 0),
                            ),
                          ],
                        ),
                        height: 100,
                        width: 180,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          gradient: LinearGradient(colors: [
                            Colors.red.shade900,
                            Colors.red.shade700
                          ]),
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(context).shadowColor,
                              blurRadius: 2,
                              offset: Offset(3, 0),
                            ),
                          ],
                        ),
                        height: 100,
                        width: 180,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          gradient: LinearGradient(colors: [
                            Colors.green.shade900,
                            Colors.green.shade700
                          ]),
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(context).shadowColor,
                              blurRadius: 2,
                              offset: Offset(3, 0),
                            ),
                          ],
                        ),
                        height: 100,
                        width: 180,
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 445,
                right: 45,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: Ink(
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.black54, width: 3),
                              color: Colors.white,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Theme.of(context).shadowColor,
                                  blurRadius: 5,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(100),
                              child: IconButton(
                                icon: new Image.asset(
                                  'assets/Images/location.png',
                                  height: 35,
                                  width: 35,
                                ),
                                onPressed: () {},
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 8),
                          child: Text(
                            'Nearby Clinic',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: Ink(
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.black54, width: 3),
                              color: Colors.white,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Theme.of(context).shadowColor,
                                  blurRadius: 4,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(100),
                              child: IconButton(
                                icon: new Image.asset(
                                  'assets/Images/insurance.png',
                                  height: 35,
                                  width: 35,
                                ),
                                onPressed: () {},
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 8),
                          child: Text(
                            'View Claims',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: Ink(
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.black54, width: 3),
                              color: Colors.white,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Theme.of(context).shadowColor,
                                  blurRadius: 4,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(100),
                              child: IconButton(
                                icon: new Image.asset(
                                  'assets/Images/clipboard.png',
                                  height: 35,
                                  width: 35,
                                ),
                                onPressed: () {},
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 8),
                          child: Text(
                            'Apply Claims',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                //),
              ),
              Positioned(
                top: 555,
                right: 105,
                //child: Align(
                //alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: Ink(
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.black54, width: 3),
                              color: Colors.white,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Theme.of(context).shadowColor,
                                  blurRadius: 4,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(100),
                              child: IconButton(
                                icon: new Image.asset(
                                  'assets/Images/desk-bell.png',
                                  height: 35,
                                  width: 35,
                                ),
                                onPressed: () {},
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 8),
                          child: Text(
                            'Requests',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 60,
                          width: 60,
                          child: Ink(
                            decoration: BoxDecoration(
                              border:
                                  Border.all(color: Colors.black54, width: 3),
                              color: Colors.white,
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                  color: Theme.of(context).shadowColor,
                                  blurRadius: 4,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(100),
                              child: IconButton(
                                icon: new Image.asset(
                                  'assets/Images/balance.png',
                                  height: 35,
                                  width: 35,
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, '/view_balance');
                                },
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 8),
                          child: Text(
                            'View Balance',
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
