import 'package:flutter/material.dart';
import 'package:claimz_app/Pages/home_page.dart';
import 'package:claimz_app/Pages/view_balance.dart';
import 'package:claimz_app/theme/theme_notifier.dart';
import 'package:claimz_app/theme/theme.dart';
import 'package:provider/provider.dart';
import 'package:claimz_app/Pages/settings.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final Map<int, Color> color = {
    50: Color.fromRGBO(48, 71, 94, .1),
    100: Color.fromRGBO(48, 71, 94, .2),
    200: Color.fromRGBO(48, 71, 94, .3),
    300: Color.fromRGBO(48, 71, 94, .4),
    400: Color.fromRGBO(48, 71, 94, .5),
    500: Color.fromRGBO(48, 71, 94, .6),
    600: Color.fromRGBO(48, 71, 94, .7),
    700: Color.fromRGBO(48, 71, 94, .8),
    800: Color.fromRGBO(48, 71, 94, .9),
    900: Color.fromRGBO(48, 71, 94, 1),
  };

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeNotifier>(
      create: (_) => ThemeNotifier(),
      child: MaterialAppWithTheme(),
    );
  }
}

class MaterialAppWithTheme extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeNotifier>(
      builder: (context, appState, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          themeMode: appState.isDark ? ThemeMode.dark : ThemeMode.light,
          home: MyHomePage(),
          routes: {
            '/view_balance': (context) => ViewBalancePage(),
            '/settings': (context) => EditSettingPage(),
          },
        );
      },
    );
  }
}
