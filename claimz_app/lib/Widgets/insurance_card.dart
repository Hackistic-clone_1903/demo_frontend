import 'package:flutter/material.dart';

class InsCard extends StatelessWidget {
  InsCard({
    this.color1,
    this.color2,
  });

  final Color color1;
  final Color color2;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        gradient: LinearGradient(colors: [color1, color2]),
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).shadowColor,
            blurRadius: 2,
            offset: Offset(3, 0),
          ),
        ],
      ),
      height: 200,
      width: 320,
    );
  }
}
