import 'package:flutter/material.dart';

import 'package:claimz_app/Widgets/custom_listtile.dart';

class EndDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.deepOrange, Colors.orangeAccent])),
            child: Container(
              child: Column(
                children: <Widget>[
                  Material(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/Images/ZenitusLogo.png',
                        width: 80,
                        height: 80,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'ClaimZ',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
          CustomListTile(Icons.person, 'Profile', () => {}),
          CustomListTile(Icons.settings, 'Settings',
              () => {Navigator.pushNamed(context, '/settings')}),
          CustomListTile(Icons.help, 'FAQ', () => {}),
          CustomListTile(Icons.logout, 'Logout', () => {}),
        ],
      ),
    );
  }
}
